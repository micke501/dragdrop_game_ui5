module.exports = function (grunt) {
	"use strict";
	/*grunt.loadNpmTasks("@sap/grunt-sapui5-bestpractice-build");
	grunt.config.merge({ compatVersion: "edge" });
	grunt.registerTask("default", [
		"clean",
		"lint",
		"build"
	]);
	grunt.loadNpmTasks("@sap/grunt-sapui5-bestpractice-test");
	grunt.registerTask("unit_and_integration_tests", ["test"]);
	grunt.config.merge({
		coverage_threshold: {
			statements: 0,
			branches: 100,
			functions: 0,
			lines: 0
		}
	});*/

	grunt.registerTask("assets", "Get all assets", function () {
		var obj = {
			"folders": []
		};
		grunt.file.recurse("webapp/assets", function (absPath, rootDir, subDir, fileName) {
			if(subDir){
				if(obj[subDir]){
					obj[subDir].push({
						"name": fileName
					})
				}else{
					obj[subDir] = [];
					obj["folders"].push({
						"key": subDir
					});
				}
			}
		});
		grunt.file.write("webapp/assets/images.json", JSON.stringify(obj));
	});


};