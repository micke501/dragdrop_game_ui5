sap.ui.define([
    "com/evora/dragdrop/game/meme/controller/BaseController",
    "sap/m/MessageToast"
], function (BaseController, MessageToast) {
    "use strict";

    return BaseController.extend("com.evora.dragdrop.game.meme.controller.RightPanel", {

        oMemeModel: null,


        onInit: function () {
            this.oMemeModel = this.getModel("meme");
            this.oLayerList = this.getView().byId("layerList");
        },

        /**
         * On press layer save context path
         * and mark layer as selected
         */
        onLayerSelect: function (oEvent) {
            var oParams = oEvent.getParameters();
            var oListItem = oEvent.getParameter("listItem"),
                oSelectedData = this.oMemeModel.getProperty("/selectedLayer") || {},
                oTextValues = this.getOwnerComponent().getDefaultTextValues();

            var oContext = oListItem.getBindingContext("meme");
            if(oContext){
                oSelectedData = oContext.getObject();
                oSelectedData.path = oContext.getPath();

                if(oSelectedData.type === "text"){
                    oTextValues = oSelectedData.style;
                }
            }
            this.oMemeModel.setProperty("/selectedLayer", oSelectedData);
            this.oMemeModel.setProperty("/textField", oTextValues);
        },

        // Todo: add drag and drop functionality


        /**
         *
         * @param oContext
         * @private
         */
        _setMemeLayerPosition: function (oContext, posX, posY) {
            var sPath = oContext.getPath();

            this.oMemeModel.setProperty(sPath + "/style/posX", posX);
            this.oMemeModel.setProperty(sPath + "/style/posY", posY);

            this.oMemeModel.setProperty("/textField/top", posY);
            this.oMemeModel.setProperty("/textField/left", posX);
        },

        /**
         *
         * @param sSrc
         * @param sText
         * @param oStyle
         * @param sType
         * @private
         */
        _addNewItem: function (sSrc, oStyle, sType) {
            var aLayers = this.oMemeModel.getProperty("/layers");
            aLayers.push({
                id: new Date().getTime(),
                type: sType,
                src: sSrc,
                style: oStyle
            });
            this.oMemeModel.setProperty("/layers", aLayers);
        }
    });
});