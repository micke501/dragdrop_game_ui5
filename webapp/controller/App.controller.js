sap.ui.define([
	"com/evora/dragdrop/game/meme/controller/BaseController"
], function (BaseController) {
	"use strict";

	return BaseController.extend("com.evora.dragdrop.game.meme.controller.App", {


		onInit: function () {
			var eventBus = sap.ui.getCore().getEventBus();
			eventBus.subscribe("RightPanel", "droppedFile", this._setNewBGPicture, this);
		},

		onExit: function () {
			if(this._oSnapshotDialog){
				this._oSnapshotDialog.destroy();
			}
		},

		/**
		 * show image list on left side
		 * @param oEvent
         */
		onPressImageIcon: function (oEvent) {
			this.getRouter().navTo("imageSelect", {});
		},

		/**
		 *
		 * @param oEvent
         */
		onPressTextIcon: function (oEvent) {
			this.getRouter().navTo("fontFormat", {});
		},

		/**
		 *
		 * @param oEvent
         */
		onPressEditIcon: function (oEvent) {
			this.getRouter().navTo("editLayer", {});
		},

		/**
		 *
		 * @param oEvent
         */
		onPressSaveIcon: function (oEvent) {
			if(!this._oFinalPicDialog){
				this._oFinalPicDialog = sap.ui.xmlfragment("com.evora.dragdrop.game.meme.view.fragments.SaveDialog", this);
				this.getView().addDependent(this._oFinalPicDialog);
			}
			this._oFinalPicDialog.open();
			//show dialog wth picture
			html2canvas(document.querySelector(".layers-wrapper")).then(function (canvas) {
				canvas.setAttribute("id", "finalCanvas");
				document.querySelector(".final-picture").appendChild(canvas);
				canvasToImage("finalCanvas", {
					name: "meme_" + new Date().getTime(),
					type: "png",
					quality: 1
				});
			}.bind(this));
		},

		/**
		 *
		 * @param oEvent
         */
		onPressResetIcon: function (oEvent) {
			var oModel = this.getModel("meme");
            oModel.setProperty("/picUri", "");
            oModel.setProperty("/layers", [{
                id: 0,
                type: "default",
                text: ""
            }]);
            oModel.setProperty("/selectedLayerPath", "");
            oModel.setProperty("/textField", this.getOwnerComponent().getDefaultTextValues());
		},

		/**
		 *
		 * @param oEvent
         */
		onPressUploadPicture: function (oEvent) {
			if(!this._oSelectPicDialog){
				this._oSelectPicDialog = sap.ui.xmlfragment("com.evora.dragdrop.game.meme.view.fragments.SelectPictureDialog", this);
				this.getView().addDependent(this._oSelectPicDialog);
			}
			this._oSelectPicDialog.open();
		},

		/**
		 * open dialog and get a webcam snapshot
		 * @param oEvent
         */
		onPressTakeNewPicture: function (oEvent) {
			if(!this._oSnapshotDialog){
				this._oSnapshotDialog = sap.ui.xmlfragment("com.evora.dragdrop.game.meme.view.fragments.SnapshotDialog", this);
				this.getView().addDependent(this._oSnapshotDialog);
			}
			this._oSnapshotDialog.open();

			this._oWebcamStream = new WebcamScanner();
			this._oWebcamStream.set({
				container: "webcam-stream",
				scanMode: "manual",
				imageFormat: "jpeg",
				jpegQuality: 100,
				width: 640,
				height: 640,
				showLogMsg: false,
				debugMode: false
			});

		},

		/**
		 *
		 * @param oEvent
         */
		onPressGetSnapshot: function (oEvent) {
			var oModel = this.getView().getModel("meme");
			this._oWebcamStream.getImage(function (img) {
				if(img){
					oModel.setProperty("/picUri", img.src);
				}
			}, true);
			this._oWebcamStream.stop();
			this._oSnapshotDialog.close();
		},

		/**
		 * reads selected file to an array
		 * @param oEvent
		 */
		onChangeSelectedFile: function (oEvent) {
			var files = oEvent.getParameter("files");
			if (files && files[0]) {
				this._newBgImage = files[0];
			}
		},

		/**
		 * read selected file to data uri
		 * @param oEvent
         */
		onPressSelectPic: function (oEvent) {
			this._uploadSelectedFile();
			this._oSelectPicDialog.close();
		},

        /**
		 * close all open dialogs
         */
		onCloseDialog: function () {
			if(this._oSnapshotDialog){
				this._oWebcamStream.stop();
				this._oSnapshotDialog.close();
			}
			if(this._oSelectPicDialog){
				this._oSelectPicDialog.close();
			}
			if(this._oFinalPicDialog){
				this._oFinalPicDialog.close();
			}
		},

		/**
		 * read file from dropped event
		 * @param sChannel
		 * @param sEvent
		 * @param oData
		 * @private
		 */
		_setNewBGPicture: function (sChannel, sEvent, oData) {
			if(sEvent === "droppedFile" && oData.file){
				this._newBgImage = oData.file;
				this._uploadSelectedFile();
			}
		},

		/**
		 * read file and set picUri in model
		 * @private
		 */
		_uploadSelectedFile: function () {
			var oModel = this.getView().getModel("meme");
			if (this._newBgImage && window.FileReader) {
				var fileReader = new window.FileReader();
				fileReader.readAsDataURL(this._newBgImage);
				fileReader.onloadend = function () {
					oModel.setProperty("/picUri", fileReader.result);
				};
			}
		}
	});
});