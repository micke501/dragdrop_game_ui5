sap.ui.define([
	"com/evora/dragdrop/game/meme/controller/BaseController",
	"com/evora/dragdrop/game/meme/model/models",
	"sap/ui/model/json/JSONModel"
], function (BaseController, models, JSONModel) {
	"use strict";

	return BaseController.extend("com.evora.dragdrop.game.meme.controller.ImageSelectPanel", {

		onInit: function () {
			var oFolderSelect = this.byId("folderSelect");
			oFolderSelect.setSelectedKey("animals");

            this.oAssetModel = this.getModel("assets");
            this.oAssetModel.loadData("assets/images.json");
            this.oAssetModel.attachRequestCompleted(function(oEvent){
				//This is called after data is loading
                this.oAssetModel.setProperty("/selectedList", []);
				this._bindImagePath(oFolderSelect.getSelectedKey());
			}.bind(this));

		},

		/**
		 *
		 * @param oEvent
         */
		onSelectFolderKey: function (oEvent) {
			var oParams = oEvent.getParameters();
			this._bindImagePath(oParams.selectedItem.getKey());
		},

		/**
		 *
		 * @param key
         * @private
         */
		_bindImagePath: function (key) {
            this.oAssetModel.setProperty("/selectedListPath", "assets/"+key+"/");
            this.oAssetModel.setProperty("/selectedList", this.oAssetModel.getProperty("/"+key));
		}



	});
});