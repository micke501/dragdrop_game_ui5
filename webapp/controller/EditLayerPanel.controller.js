sap.ui.define([
	"com/evora/dragdrop/game/meme/controller/BaseController"
], function (BaseController) {
	"use strict";

	return BaseController.extend("com.evora.dragdrop.game.meme.controller.EditLayerPanel", {

		onInit: function () {

		},

		onExit: function () {
			if (this.oColorPickerPopover) {
				this.oColorPickerPopover.destroy();
			}
		},

        /**
		 * Deletes selected layer
         * @param oEvent
         */
        onPressDelete :function (oEvent) {
            var oMemeModel = this.getModel("meme"),
                oSelectedLayerData = oMemeModel.getProperty("/selectedLayer");

            if(oSelectedLayerData.type !== "default"){
                var aLayers = oMemeModel.getProperty("/layers"),
                    path = oSelectedLayerData.path.substring(oSelectedLayerData.path.lastIndexOf('/') +1),
                    idx = parseInt(path);

                var rm = aLayers.splice(idx, 1);
                oMemeModel.setProperty("/selectedLayer", {});
			}
        },

        /**
		 * Syncronize the changes in the Model
         * @param oEvent
         * @param sProperty
         */
        onChangePosition : function (oEvent, sProperty) {
			var sNewValue = oEvent.getParameter("value");
			var oMemeModel = this.getModel("meme"),
				oSelectedLayerData = oMemeModel.getProperty("/selectedLayer");

			if(oSelectedLayerData.type !== "default"){
                var sValue = parseInt(sNewValue);
				if(sProperty === "width"){
					sValue += "px";
				}
                oMemeModel.setProperty(oSelectedLayerData.path+"/style/"+sProperty, sValue);
			}
        },

        /**
		 * remove css style unit from string
		 * and format to slider number
         * @param sValue
         * @returns {*}
         */
        getCSSNumber: function (sValue) {
			if(sValue && sValue.indexOf("px")){
                return parseFloat(sValue.split("px")[0]);
			}
			return sValue;
        }


	});
});