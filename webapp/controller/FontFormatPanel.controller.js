sap.ui.define([
    "com/evora/dragdrop/game/meme/controller/BaseController",
    "com/evora/dragdrop/game/meme/model/models",
    "sap/ui/model/json/JSONModel"
], function (BaseController, models, JSONModel) {
    "use strict";

    return BaseController.extend("com.evora.dragdrop.game.meme.controller.FontFormatPanel", {

        /**
         * set default values for font simple form
         */
        onInit: function () {

        },

        /**
         * life cycle event after view rendering
         */
        onAfterRendering: function () {
            this.oMemeModel = this.getModel("meme");
            var oSampleText = this.getView().byId("sampleText");
            this.sampleBox = document.getElementById(oSampleText.getId());
            if(this.sampleBox){
                this._setSampleTextBox();
            }
        },

        /**
         *
         * @param oEvent
         */
        onPressFontStyle: function (oEvent, sProperty) {
            var oParams = oEvent.getParameters();
            var oTextData = this.oMemeModel.getProperty("/textField");

            if(oTextData.hasOwnProperty(sProperty)){
                this.oMemeModel.setProperty("/textField/"+sProperty, oParams.pressed);
                this._setSampleTextBox();
            }
        },

        /**
         *
         * @param oEvent
         */
        onChangeFontField: function (oEvent) {
            this._setSampleTextBox();
        },

        /**
         * on add new text
         * @param oEvent
         */
        onPressAddText: function (oEvent) {
            this._addTextItem(this.oMemeModel.getProperty("/textField"));
        },

        onPressColorPicker: function (oEvent) {
            if(!this.oColorPickerPopover){
                this.oColorPickerPopover = this.getColorPicker(this.onChangeColor);
            }
            this.oColorPickerPopover.openBy(oEvent.getSource());
        },

        onChangeColor: function (oEvent) {
            var color = oEvent.getParameter("colorString");
            this.oMemeModel.setProperty("/textField/textColor", color);
            this._setSampleTextBox();
        },

        onExit: function () {
            if (this.oColorPickerPopover) {
                this.oColorPickerPopover.destroy();
            }
        },

        /**
         *
         * @param sSrc
         * @param sText
         * @param oStyle
         * @param sType
         * @private
         */
        _addTextItem: function (oData) {
            var oSelectedLayerData = this.oMemeModel.getProperty("/selectedLayer");

            if(oData.text){
                if(oSelectedLayerData && (oSelectedLayerData.type === "text")){
                    this.oMemeModel.setProperty(oSelectedLayerData.path+"/text", oData.text);
                    this.oMemeModel.setProperty(oSelectedLayerData.path+"/style", oData);
                }else{
                    var aLayers = this.oMemeModel.getProperty("/layers");
                    aLayers.push({
                        id: new Date().getTime(),
                        type: "text",
                        text: oData.text,
                        style: oData
                    });
                    this.oMemeModel.setProperty("/layers", aLayers);
                    this.oMemeModel.setProperty("/textField", this.getOwnerComponent().getDefaultTextValues());
                }
            }
        },

        /**
         * set in vanilla JS inline styles for sample text
         * @private
         */
        _setSampleTextBox: function () {
            var oData = this.oMemeModel.getProperty("/textField");
            this.sampleBox.style.color = oData.textColor;
            this.sampleBox.style.fontFamily = oData.fontFamily;
            this.sampleBox.style.fontStyle =  oData.isItalic ? "italic" : "normal";
            this.sampleBox.style.fontWeight = oData.isBold ? "bold" : "normal";
            this.sampleBox.style.textDecoration = oData.isUnderlined ? "underline" : "none";
        }

    });
});