sap.ui.define([
	"sap/ui/model/json/JSONModel",
	"sap/ui/Device"
], function (JSONModel, Device) {
	"use strict";

	return {

		createDeviceModel: function () {
			var oModel = new JSONModel(Device);
			oModel.setDefaultBindingMode("OneWay");
			return oModel;
		},

		createOneWayHelperModel: function (obj) {
			var oModel = new JSONModel(obj);
			oModel.setDefaultBindingMode("OneWay");
			return oModel;
		},

		createTwoWayHelperModel: function (obj) {
			var oModel = new JSONModel(obj);
			oModel.setDefaultBindingMode("TwoWay");
			return oModel;
		}

	};
});