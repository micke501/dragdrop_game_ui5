/* global QUnit */
QUnit.config.autostart = false;

sap.ui.getCore().attachInit(function () {
	"use strict";

	sap.ui.require([
		"com/evora/dragdrop/game/meme/test/integration/AllJourneys"
	], function () {
		QUnit.start();
	});
});