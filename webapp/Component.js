sap.ui.define([
	"sap/ui/core/UIComponent",
	"sap/ui/Device",
	"com/evora/dragdrop/game/meme/model/models"
], function (UIComponent, Device, models) {
	"use strict";

	return UIComponent.extend("com.evora.dragdrop.game.meme.Component", {

		metadata: {
			manifest: "json"
		},

		/**
		 * The component is initialized by UI5 automatically during the startup of the app and calls the init method once.
		 * @public
		 * @override
		 */
		init: function () {
			// call the base component's init function
			UIComponent.prototype.init.apply(this, arguments);

			// enable routing
			this.getRouter().initialize();

			// set the device model
			this.setModel(models.createDeviceModel(), "device");

			//create asset model
			this.setModel(models.createOneWayHelperModel(), "assets");


			// set main view json model
			var obj = {
				picUri: "",
                textField: this.getDefaultTextValues,
				layers: [{
					id: 0,
					type: "default",
					text: "",
					style: {}
				}]
			};
			this.setModel(models.createTwoWayHelperModel(obj), "meme");
		},

        /**
		 *
         * @returns {{top: number, left: number, text: string, fontFamily: string, textColor: string, fontSize: number, isItalic: boolean, isBold: boolean, isUnderlined: boolean}}
         */
		getDefaultTextValues: function () {
			return {
                top:10,
                left:10,
                text: "",
                fontFamily: "arial",
                textColor: "#000000",
                fontSize: 16,
                isItalic: false,
                isBold: false,
                isUnderlined: false
            };
        }
	});
});