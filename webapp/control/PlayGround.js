sap.ui.define([
    "sap/m/ListItemBase"
], function (ListItemBase) {
    return ListItemBase.extend("com.evora.dragdrop.game.meme.control.PlayGround", {
        metadata: {
            properties: {
                src: {type: "sap.ui.core.URI", group: "Appearance", defaultValue: null},
                width: {type: "sap.ui.core.CSSSize", group: "Appearance", defaultValue: null},
                top: {type: "int", group: "Appearance", defaultValue: null},
                left: {type: "int", group: "Appearance", defaultValue: null},
                text: {type: "string", group: "Misc", defaultValue: null},
                textColor: {type: "sap.ui.core.CSSColor", group: "Appearance", defaultValue: "white"},
                fontSize: {type: "int", group: "Appearance", defaultValue: 12},
                fontFamily: {type: "string", group: "Appearance", defaultValue: "Arial,Helvetica,sans-serif"},
                isItalic: {type: "boolean", group: "Appearance", defaultValue: false},
                isBold: {type: "boolean", group: "Appearance", defaultValue: false},
                isUnderlined : {type: "boolean", group: "Appearance", defaultValue: false}
            }
        }
    });
});