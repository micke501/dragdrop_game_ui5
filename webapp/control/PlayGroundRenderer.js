sap.ui.define([
        "sap/ui/core/Renderer",
        "sap/m/ListItemBaseRenderer"
    ],
    function (Renderer,ListItemBaseRenderer) {
        "use strict";

        var oPlayGroundRenderer = {};
        oPlayGroundRenderer.render = function (oRm, oControl) {
            var iTop = oControl.getTop() || 0,
                iLeft = oControl.getLeft() || 0,
                sText = oControl.getText(),
                sTextColor = oControl.getTextColor(),
                iFontSize = oControl.getFontSize(),
                sFontFamily = oControl.getFontFamily(),
                sFontStyle = oControl.getIsItalic() ? "italic" : "normal",
                sFontWeight = oControl.getIsBold() ? "bold" : "normal",
                sTextDecoration = oControl.getIsUnderlined() ? "underline" : "none";

            oRm.write("<", "li");
            oRm.writeControlData(oControl);
            oRm.addClass("layer-item");
            oRm.addStyle("position", "absolute");
            oRm.addStyle("top", iTop + "px");
            oRm.addStyle("left", iLeft + "px");

            if (oControl.getSelected()) {
                oRm.addClass("sapMLIBSelected");
            }

            // attributes
            ListItemBaseRenderer.renderTooltip(oRm, oControl);
            ListItemBaseRenderer.renderTabIndex(oRm, oControl);

            oRm.writeClasses();
            oRm.writeStyles();
            oRm.write(">");

            //for selecting item
            ListItemBaseRenderer.renderContentFormer(oRm, oControl);

            if (sText !== null && sText !== "") {
                oRm.write("<", "span");
                oRm.writeControlData(oControl);
                oRm.addClass("object");
                oRm.addClass("clipArtText");
                oRm.addStyle("color", sTextColor);
                oRm.addStyle("font-size", iFontSize + "px");
                oRm.addStyle("font-family", sFontFamily);

                oRm.addStyle("font-style", sFontStyle);
                oRm.addStyle("font-weight", sFontWeight);
                oRm.addStyle("text-decoration", sTextDecoration);
                oRm.writeClasses();
                oRm.writeStyles();
                oRm.write(">");
                oRm.write(sText);
                oRm.write("</span>");
            } else {
                oRm.write("<", "img");
                oRm.writeControlData(oControl);
                oRm.writeAttributeEscaped("src", oControl.getSrc());
                oRm.writeAttributeEscaped("width", oControl.getWidth());
                oRm.addClass("object");
                oRm.addClass("clipArtImage");
                oRm.writeClasses();
                oRm.write(">");
                oRm.write("</img>");
            }

            oRm.write("</li>");
        };
        return oPlayGroundRenderer;

    },true

);